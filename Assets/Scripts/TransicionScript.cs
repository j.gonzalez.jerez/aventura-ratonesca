﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransicionScript  : MonoBehaviour {

	public GameObject PanelHabil;
	public GameObject PanelAtaque;
	public GameObject PanelPrincipal;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Huir(){
		SceneManager.LoadScene(0);
	}

	public void Atacar(){
		Debug.Log("atacar");
		PanelAtaque.SetActive(true);
		PanelPrincipal.SetActive(false);
	}

	public void Defender(){
		Debug.Log("defender");
	}

	public void Habilidad(){
		Debug.Log("habilidades");
		PanelHabil.SetActive(true);
		PanelPrincipal.SetActive(false);
	}


}
