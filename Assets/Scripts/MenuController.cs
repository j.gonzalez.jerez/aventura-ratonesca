﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController  : MonoBehaviour {

	public GameObject JugarD;
	public GameObject CreditosD;
	public GameObject SalirD;
	public GameObject PanelPrincipalD;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Salir(){
		Debug.Log("Saliste");
	}

	public void Jugar(){
		SceneManager.LoadScene(0);
	}


	public void Creditos(){
		CreditosD.SetActive(true);
		PanelPrincipalD.SetActive(false);
	}

	public void Volver(){
		CreditosD.SetActive(false);
		PanelPrincipalD.SetActive(true);
	}


}